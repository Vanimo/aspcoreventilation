﻿using System;
using System.Collections.Generic;

namespace AspCoreVentilation.Hub
{
	public interface IConnectionManager
	{
		void AddConnection(String userName, String connectionID);

		void Removeconnection(String connectionID);

		HashSet<String> GetConnections(String username);

		IEnumerable<String> OnlineUsers { get; }
	}
}
