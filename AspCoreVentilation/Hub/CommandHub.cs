﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;
using System.Threading.Tasks;
using System.Timers;
using AspCoreVentilation.Controllers;
using Microsoft.AspNetCore.SignalR;

namespace AspCoreVentilation.Hub
{
	public class CommandHub : Microsoft.AspNetCore.SignalR.Hub
	{
		public static readonly CultureInfo LocalCultureInfo = CultureInfo.GetCultureInfo("nl-BE");

		public CommandHub()
		{
		}

		/// <summary>
		/// Receives a command from a client
		/// </summary>
		/// <param name="cmd"></param>
		/// <returns></returns>
		public async Task SendCommand(Int32 id, String cmd, String argument)
		{
			await GlobalController.HandleCommand(id, cmd, argument);
		}

		public static async Task NotifyControllerState(IHubContext<CommandHub> hubContext, VentilationRelayController controller)
		{
			// Server forwards message to all clients

			var sbMessage = new StringBuilder();

			sbMessage.Append("<b>").Append(controller.IsInManualState ? "Manueel" : "Automatisch").Append("</b>"); // Manual : Automatic

			sbMessage.Append(" op stand "); // at level
			sbMessage.Append("<b>").Append(FormatState(controller.CurrentState)).Append("</b>");

			if (controller.IsInTemporaryState)
			{ 
				sbMessage.Append(" tot " + FormatDT(controller.ExpectedEndOfTemporaryState) + "."); // until DT
			}
			else
			{
				sbMessage.Append(" tot de volgende geplande wijziging."); // until the next scheduled change
			}

			await hubContext.Clients.All.SendAsync("ReceiveUpdate", sbMessage.ToString());
		}

		public static async Task NotifyNextScheduleState(IHubContext<CommandHub> hubContext, VentilationSchedulingController.ScheduleState nextState)
		{
            var sbMessage = new StringBuilder();
			sbMessage.Append("Volgende geplande wijziging is <b>"); // Next scheduled change is on

			if (nextState == null)
            {
                sbMessage.Append("</b> ongekend.<b>"); // unknown
			}
            else
			{
				sbMessage.Append(FormatDoW(nextState.Day)); // Day of week
                sbMessage.Append("</b> om <b>"); // at
                sbMessage.Append(FormatTS(nextState.TriggerTime)); // Time of day
                sbMessage.Append("</b> naar <b>"); // to
                sbMessage.Append(FormatState(nextState.DesiredState));
                sbMessage.Append("</b>.");
            }

            await hubContext.Clients.All.SendAsync("ReceiveNextScheduleState", sbMessage.ToString());
		}

		public static async Task NotifyAvailableSchedules(IHubContext<CommandHub> hubContext, IEnumerable<string> schedules)
		{
			var sbMessage = new StringBuilder();

			bool first = true;
			string activeSchedule = null;

			foreach (var schedule in schedules)
			{
				if (first)
				{
					first = false;
					activeSchedule = schedule;
					continue;
				}

				var sChecked = schedule == activeSchedule ? "checked" : string.Empty;
				sbMessage.Append($@"<input type=""radio"" id=""{schedule}"" name=""activeScheduleSelection"" value=""{schedule}"" {sChecked} />");
				sbMessage.Append($@"<label for=""{schedule}"">{schedule}</label>");
				sbMessage.AppendLine("<br />");
			}

			await hubContext.Clients.All.SendAsync("ReceiveAvailableSchedules", sbMessage.ToString());
		}

		public static async Task NotifyActiveSchedule(IHubContext<CommandHub> hubContext, string schedule)
		{
			await hubContext.Clients.All.SendAsync("ReceiveActiveSchedule", schedule);
		}

		public static async Task NotifyConfigFileContent(IHubContext<CommandHub> hubContext, string content)
        {
            await hubContext.Clients.All.SendAsync("ReceiveConfigContent", content);
        }

		private static String FormatDT(DateTime dt)
		{
			return dt.ToString("F", LocalCultureInfo);
		}

		private static String FormatTS(TimeSpan ts)
		{
			return ts.ToString("g", LocalCultureInfo);
		}

		private static String FormatDoW(DayOfWeek dow)
        {
			// Translate since the formatters are obsolete

			switch(dow)
            {
				case DayOfWeek.Monday: return "maandag";
				case DayOfWeek.Tuesday: return "dinsdag";
				case DayOfWeek.Wednesday: return "woensdag";
				case DayOfWeek.Thursday: return "donderdag";
				case DayOfWeek.Friday: return "vrijdag";
				case DayOfWeek.Saturday: return "zaterdag";
				case DayOfWeek.Sunday: return "zondag";
				default: return string.Empty;
            }
        }

		private static String FormatState(VentilationRelayController.OperatingStates state)
        {
			switch (state)
            {
				case VentilationRelayController.OperatingStates.None: return "ongekend"; // unknown
				case VentilationRelayController.OperatingStates.Off: return "uit";
				case VentilationRelayController.OperatingStates.Low: return "laag";
				case VentilationRelayController.OperatingStates.Med: return "matig";
				case VentilationRelayController.OperatingStates.High: return "hoog";
				default: return string.Empty;
            }
        }
	}
}
