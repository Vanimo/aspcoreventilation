﻿using System;
using System.Collections.Generic;

namespace AspCoreVentilation.Hub
{
	public class ConnectionManager : IConnectionManager
	{
		private static Object _userMapLock = new Object();
		private static Dictionary<String, HashSet<String>> _userMap = new Dictionary<String, HashSet<String>>();

		public void AddConnection(String userName, String connectionID)
		{
			lock (_userMapLock)
			{
				if (!_userMap.ContainsKey(userName))
				{
					_userMap[userName] = new HashSet<String>();
				}

				_userMap[userName].Add(connectionID);
			}
		}

		public void Removeconnection(String connectionID)
		{
			lock (_userMapLock)
			{
				foreach (var userConnections in _userMap)
				{
					userConnections.Value.Remove(connectionID);
				}
			}
		}

		public HashSet<String> GetConnections(String username)
		{
			HashSet<String> connections = null;

			lock (_userMapLock)
			{
				if (_userMap.ContainsKey(username))
				{
					connections = _userMap[username];
				}
			}

			return connections;
		}

		public IEnumerable<String> OnlineUsers
		{
			get { return _userMap.Keys; }
		}
	}
}
