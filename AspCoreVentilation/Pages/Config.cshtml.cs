﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AspCoreVentilation.Pages
{
	public class ConfigModel : PageModel
	{
		private readonly ILogger<ConfigModel> _logger;

		public ConfigModel(ILogger<ConfigModel> logger)
		{
			_logger = logger;
		}

		public void OnGet()
		{

		}
	}
}
