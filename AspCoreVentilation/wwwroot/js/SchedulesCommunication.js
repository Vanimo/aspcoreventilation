﻿"use strict";

var connection = new signalR.HubConnectionBuilder().withUrl("/communication").withAutomaticReconnect().build();

////Disable send button until connection is established
//document.getElementById("buttonBoost10").disabled = true;

connection.start().then(function () {
	connectionStateChanged(0);
	connection.invoke("SendCommand", 0, "GetSchedules", "");
	connection.invoke("SendCommand", 0, "GetActiveSchedule", "");
}).catch(function (err) {
	return console.error(err.toString());
});

connection.onreconnecting(function (error) {
	connectionStateChanged(2);
});

connection.onreconnected(function (connectionId) {
	connectionStateChanged(1);
	connection.invoke("SendCommand", 0, "GetState", "");
});

connection.onclose(function (error) {
	connectionStateChanged(4);
});

function connectionStateChanged(state) {
	var stateToText = { 0: 'Connecting', 1: 'Connected', 2: 'Reconnecting', 4: 'Disconnected' };
	var stateToColour = { 0: 'orange', 1: 'green', 2: 'orange', 4: 'red' };
	document.getElementById("connection").innerHTML = stateToText[state];
	document.getElementById("connection").setAttribute("style", "color:" + stateToColour[state]);
};

connection.on("ReceiveActiveSchedule", function (message) {
	document.getElementById("activeSchedule").innerHTML = "Actief schema: <b>" + message + "</b>";
	connectionStateChanged(1);
});

function sendSelectedSchedule() {

	var selections = document.getElementsByName("activeScheduleSelection");

	for (var i = 0; i < selections.length; i++) {
		if (selections[i].checked) {
			connection.invoke("SendCommand", 0, "SetSchedule", selections[i].value);
			break;
		}
	}

	// Since we send the command using signalR, block the form from further actions.
	return false;
};

connection.on("ReceiveAvailableSchedules", function (message) {
	document.getElementById("activeValueSelectionDiv").innerHTML = message;
	connectionStateChanged(1);
});
