﻿"use strict";

var connection = new signalR.HubConnectionBuilder().withUrl("/communication").withAutomaticReconnect().build();

////Disable send button until connection is established
//document.getElementById("buttonBoost10").disabled = true;

connection.start().then(function () {
	connectionStateChanged(0);
	connection.invoke("SendCommand", 0, "GetConfig", "");
}).catch(function (err) {
	return console.error(err.toString());
});

connection.onreconnecting(function (error) {
	connectionStateChanged(2);
});

connection.onreconnected(function (connectionId) {
	connectionStateChanged(1);
	connection.invoke("SendCommand", 0, "GetState", "");
});

connection.onclose(function (error) {
	connectionStateChanged(4);
});

function connectionStateChanged(state) {
	var stateToText = { 0: 'Connecting', 1: 'Connected', 2: 'Reconnecting', 4: 'Disconnected' };
	var stateToColour = { 0: 'orange', 1: 'green', 2: 'orange', 4: 'red' };
	document.getElementById("connection").innerHTML = stateToText[state];
	document.getElementById("connection").setAttribute("style", "color:" + stateToColour[state]);
};

connection.on("ReceiveConfigContent", function (message) {
	document.getElementById("configXml").innerHTML = message;
	connectionStateChanged(1);
});

function sendEditedConfig() {
	var elem = document.getElementById("configXml");
	var text = elem.value;
	connection.invoke("SendCommand", 0, "SaveConfig", text);

	// Since we send the command using signalR, block the form from further actions.
	return false;
};
