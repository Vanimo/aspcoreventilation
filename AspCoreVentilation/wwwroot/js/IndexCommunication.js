﻿"use strict";

var connection = new signalR.HubConnectionBuilder().withUrl("/communication").withAutomaticReconnect().build();

////Disable send button until connection is established
//document.getElementById("buttonBoost10").disabled = true;

connection.on("ReceiveUpdate", function (message) {
	var msg = message.replace("&amp;", "&").replace("&lt;", "<").replace("&gt;", ">");
	document.getElementById("currentState").innerHTML = msg;
	connectionStateChanged(1);
});

connection.on("ReceiveNextScheduleState", function (message) {
	document.getElementById("nextScheduledState").innerHTML = message;
	connectionStateChanged(1);
});

connection.start().then(function () {
	connectionStateChanged(0);
	connection.invoke("SendCommand", 0, "GetState", "");
}).catch(function (err) {
	return console.error(err.toString());
});

connection.onreconnecting(function (error) {
	connectionStateChanged(2);
});

connection.onreconnected(function (connectionId) {
	connectionStateChanged(1);
	connection.invoke("SendCommand", 0, "GetState", "");
});

connection.onclose(function (error) {
	connectionStateChanged(4);
});

function connectionStateChanged(state) {
	var stateToText = { 0: 'Connecting', 1: 'Connected', 2: 'Reconnecting', 4: 'Disconnected' };
	var stateToColour = { 0: 'orange', 1: 'green', 2: 'orange', 4: 'red' };
	document.getElementById("connection").innerHTML = stateToText[state];
	document.getElementById("connection").setAttribute("style", "color:" + stateToColour[state]);
};

function setConfiguration(event) {
	connection.invoke("SetConfiguration", event.value).catch(function (err) {
		return console.error(err.toString());
	});
	event.preventDefault();
}

function addConfigButton(value) {
	var button = document.createElement("button");
	button.setAttribute("id", value);
	button.setAttribute("value", value);
	button.addEventListener(setConfiguration);

	document.getElementById("ScheduleList").appendChild(button);
}

document.getElementById("buttonHighBoost10").addEventListener("click", function (event) {
	connection.invoke("SendCommand", 0, "HighBoost", "600").catch(function (err) {
		return console.error(err.toString());
	});
	event.preventDefault();
});

document.getElementById("buttonHighBoost30").addEventListener("click", function (event) {
	connection.invoke("SendCommand", 0, "HighBoost", "1800").catch(function (err) {
		return console.error(err.toString());
	});
	event.preventDefault();
});

document.getElementById("buttonHighBoost60").addEventListener("click", function (event) {
	connection.invoke("SendCommand", 0, "HighBoost", "3600").catch(function (err) {
		return console.error(err.toString());
	});
	event.preventDefault();
});

document.getElementById("buttonMedBoost10").addEventListener("click", function (event) {
	connection.invoke("SendCommand", 0, "MedBoost", "600").catch(function (err) {
		return console.error(err.toString());
	});
	event.preventDefault();
});

document.getElementById("buttonMedBoost30").addEventListener("click", function (event) {
	connection.invoke("SendCommand", 0, "MedBoost", "1800").catch(function (err) {
		return console.error(err.toString());
	});
	event.preventDefault();
});

document.getElementById("buttonMedBoost60").addEventListener("click", function (event) {
	connection.invoke("SendCommand", 0, "MedBoost", "3600").catch(function (err) {
		return console.error(err.toString());
	});
	event.preventDefault();
});


document.getElementById("buttonOff").addEventListener("click", function (event) {
	connection.invoke("SendCommand", 0, "Temporary", "0").catch(function (err) {
		return console.error(err.toString());
	});
	event.preventDefault();
});

document.getElementById("buttonLow").addEventListener("click", function (event) {
	connection.invoke("SendCommand", 0, "Temporary", "1").catch(function (err) {
		return console.error(err.toString());
	});
	event.preventDefault();
});

document.getElementById("buttonMed").addEventListener("click", function (event) {
	connection.invoke("SendCommand", 0, "Temporary", "2").catch(function (err) {
		return console.error(err.toString());
	});
	event.preventDefault();
});

document.getElementById("buttonHigh").addEventListener("click", function (event) {
	connection.invoke("SendCommand", 0, "Temporary", "3").catch(function (err) {
		return console.error(err.toString());
	});
	event.preventDefault();
});