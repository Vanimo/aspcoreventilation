﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Timers;
using AspCoreVentilation.Hub;
using Microsoft.AspNetCore.SignalR;

namespace AspCoreVentilation.Controllers
{
	public static class GlobalController
	{
		private static readonly ConcurrentDictionary<Int32, IController> _schedulingControllers = new ConcurrentDictionary<Int32, IController>();

		private static IHubContext<CommandHub> _hubContext;

		static GlobalController()
		{
		}

		public static void SetHubContext(IHubContext<CommandHub> hubContext)
		{
			_hubContext = hubContext;
		}

		public static void RegisterController(Int32 id, IController controller)
		{
			if (_schedulingControllers.ContainsKey(id))
			{
				throw new Exception("Incorrect configuration, a controller with that ID (" + id + ") is already registered");
			}

			_schedulingControllers[id] = controller;
		}

		public static async Task SendControllerUpdate(VentilationRelayController controller)
		{
			await CommandHub.NotifyControllerState(_hubContext, controller);
		}

		public static async Task SendNextScheduleState(VentilationSchedulingController.ScheduleState nextState)
        {
			await CommandHub.NotifyNextScheduleState(_hubContext, nextState);
        }

        public static async Task SendConfigFileContent(string content)
        {
            await CommandHub.NotifyConfigFileContent(_hubContext, content);
        }

        public static async Task SendAvailableSchedules(IEnumerable<string> schedules)
        {
	        await CommandHub.NotifyAvailableSchedules(_hubContext, schedules);
        }

		public static async Task SendActiveSchedule(string schedule)
		{
			await CommandHub.NotifyActiveSchedule(_hubContext, schedule);
		}

		public static async Task HandleCommand(Int32 ID, String command, String argument)
		{
            switch (command)
            {
				case "GetConfig":
                    var content = ConfigController.GetConfigFileContent();
                    await SendConfigFileContent(content);
                    return;
				case "SaveConfig":
					ConfigController.OverwriteConfig(argument);
                    foreach (var kvp in _schedulingControllers)
                    {
                        kvp.Value.LoadConfig(argument);
                    }
                    return;
			}

            if (_schedulingControllers.TryGetValue(ID, out var controller))
            {
	            await controller.HandleCommand(command, argument);
            }
		}
	}
}
