﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace AspCoreVentilation.Controllers.Models
{
    [XmlRoot("Settings")]
    public class XmlSettings
    {
        [XmlArray("Schedules")]
        [XmlArrayItem("Schedule")]
        public XmlSchedule[] Schedules { get; set; }

        public string LastActiveSchedule { get; set; }
    }

    public class XmlSchedule
    {
        public string Name { get; set; }

        [XmlArray("Triggers")]
        [XmlArrayItem("Trigger")]
        public XmlScheduleTrigger[] Triggers;
    }

    public class XmlScheduleTrigger
    {
        [XmlIgnore]
        public TimeSpan TriggerTime { get; set; }

        [XmlElement("TriggerTime")]
        public string StringTriggerTime
        {
            get => TriggerTime.ToString("c");
            set
            {
                TimeSpan parsedValue;
                if (TimeSpan.TryParse(value, out parsedValue))
                {
                    TriggerTime = parsedValue;
                }
            }
        }

        public Int32 Level { get; set; }

        [XmlArray("DaysOfWeek")]
        [XmlArrayItem("Day")]
        public Int32[] DaysOfWeek { get; set; }
    }
}
