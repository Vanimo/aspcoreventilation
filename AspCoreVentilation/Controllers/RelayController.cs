﻿using System;
using System.Collections.Generic;
using System.Device.Gpio;
using System.Linq;
using System.Threading.Tasks;

namespace AspCoreVentilation.Controllers
{
	public class RelayController : IController
	{
		private readonly Int32 _relay;

		private readonly GpioController _controller;

		public RelayController(Int32 relay)
		{
			_relay = relay;

			_controller = new GpioController(PinNumberingScheme.Board);
			_controller.OpenPin(_relay, PinMode.Output);
			_controller.Write(_relay, PinValue.High);
		}

		public async Task HandleCommand(String command, String argument)
		{
			switch (command)
			{
				case "Activate":
				case "High":
					_controller.Write(_relay, PinValue.High);
					break;
				case "Deactivate":
				case "Low":
					_controller.Write(_relay, PinValue.Low);
					break;
			}
		}

        public void LoadConfig(string config)
        {
            
        }
    }
}
