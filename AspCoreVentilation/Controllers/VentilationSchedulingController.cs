﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Timers;
using AspCoreVentilation.Controllers.Models;

namespace AspCoreVentilation.Controllers
{
	public class VentilationSchedulingController : IController
	{
		private VentilationRelayController _ventilationController;

		private readonly Timer _updateTimer;

		private Schedule ActiveSchedule;
		private Dictionary<string, Schedule> AvailableSchedules;

		private ScheduleState _currentState;
		private ScheduleState _nextState;

		private DateTime _boostedEnd;

		public VentilationSchedulingController(VentilationRelayController ventilation)
		{
			_ventilationController = ventilation;

			_updateTimer = new Timer(1000d);
			_updateTimer.Elapsed += _updateTimerOnElapsed;
			_updateTimer.AutoReset = true;
			_updateTimer.Enabled = true;

			LoadConfig();
			EnsureScheduleAvailable();
		}

		public void LoadConfig()
		{
			var config = ConfigController.GetConfigFileContent();
			LoadConfig(config);
		}

		public void LoadConfig(string config)
		{
			AvailableSchedules = ConfigController.GetSavedSchedules(config, out string lastSchedule);
			SetActiveSchedule(lastSchedule);
		}

		private void EnsureScheduleAvailable()
		{
			if (AvailableSchedules == null || !AvailableSchedules.Any())
			{
				AvailableSchedules = new Dictionary<String, Schedule>()
				{
					{"default", GetDefaultSchedule()}
				};
			}
		}

		private void SetActiveSchedule(string name)
		{
			ActiveSchedule = GetSchedule(name);

			_currentState = FindCurrentState(DateTime.Now);
			_nextState = FindNextState(_currentState);

			_ventilationController.SetScheduledState(_currentState.DesiredState);
		}

		private Schedule GetSchedule(string name)
		{
			EnsureScheduleAvailable();
			if (name != null && AvailableSchedules.ContainsKey(name))
			{
				return AvailableSchedules[name];
			}

			if (AvailableSchedules.Any())
			{
				return AvailableSchedules.First().Value;
			}

			return GetDefaultSchedule();
		}

		private ScheduleState FindCurrentState(DateTime dt)
		{
			var targetDay = dt.DayOfWeek;
			var targetTod = dt.TimeOfDay;

			ScheduleState curState = null;
			DayOfWeek dayToTry = targetDay;

			do
			{
				var theDaySchedule = ActiveSchedule.SchedulePerDay[dayToTry];

				if (dayToTry != targetDay)
				{
					// Any state before today is the current schedule, if it's null we try the day before					
					if (theDaySchedule != null && theDaySchedule.Count > 0)
					{
						for (int i = theDaySchedule.Count - 1; i >= 0; i--)
						{
							if (theDaySchedule[i] != null)
							{
								curState = theDaySchedule[i];
								break;
							}
						}
					}
				}
				else
				{
					foreach (var state in theDaySchedule)
					{
						if (state.TriggerTime <= targetTod)
						{
							curState = state;
						}
						else
						{
							break;
						}
					}
				}

				dayToTry = GetPreviousDay(dayToTry);
			} while (curState == null && dayToTry != targetDay);

			return curState ?? GetEmptyScheduleState(targetDay);
		}

		private DayOfWeek GetNextDay(DayOfWeek day)
		{
			var iDay = (int)day;
			iDay++;

			if (iDay > 6)
			{
				iDay = 0;
			}

			return (DayOfWeek)iDay;
		}

		private DayOfWeek GetPreviousDay(DayOfWeek day)
		{
			var iDay = (int)day;
			iDay--;

			if (iDay < 0)
			{
				iDay = 0;
			}

			return (DayOfWeek)iDay;
		}

		private ScheduleState FindNextState(ScheduleState s)
		{
			var targetDay = s.Day;
			var targetTod = s.TriggerTime;

			ScheduleState nextState = null;
			DayOfWeek dayToTry = targetDay;

			do
			{
				var theDaySchedule = ActiveSchedule.SchedulePerDay[dayToTry];

				foreach (var state in theDaySchedule)
				{
					// Any state after today is the next schedule, if it's null we try the day after
					if (state != null && (dayToTry != targetDay || state.TriggerTime > targetTod))
					{
						nextState = state;
						break;
					}
				}

				dayToTry = GetNextDay(dayToTry);
			} while (nextState == null && dayToTry != targetDay);

			return nextState ?? GetEmptyScheduleState(GetNextDay(targetDay));
		}

		private async void _updateTimerOnElapsed(Object sender, ElapsedEventArgs e)
		{
			var now = DateTime.Now;
			var today = now.DayOfWeek;
			var timeOfDay = now.TimeOfDay;

			if (_boostedEnd != DateTime.MinValue && _boostedEnd < now)
			{
				// Disable the boosted state
				_boostedEnd = DateTime.MinValue;
				await _ventilationController.SetTemporaryState(VentilationRelayController.OperatingStates.None, DateTime.MinValue);
			}

			if (_nextState != null && _nextState.Day == today && _nextState.TriggerTime < timeOfDay)
			{
				// Set the new scheduled state
				_currentState = _nextState;
				_nextState = FindNextState(_currentState);

				await _ventilationController.SetScheduledState(_currentState.DesiredState);

				await GlobalController.SendNextScheduleState(_nextState);
			}
		}

		private static Schedule GetDefaultSchedule()
		{
			var schedule = new Schedule();
			schedule.SchedulePerDay[DayOfWeek.Sunday].Add(new ScheduleState()
			{
				Day = DayOfWeek.Sunday,
				TriggerTime = TimeSpan.Zero,
				DesiredState = VentilationRelayController.OperatingStates.Off
			});

			return schedule;
		}

		private static ScheduleState GetEmptyScheduleState(DayOfWeek day)
		{
			return new ScheduleState()
			{
				Day = day,
				TriggerTime = new TimeSpan(0, 0, 0),
				DesiredState = VentilationRelayController.OperatingStates.Off
			};
		}

		public class ScheduleState
		{
			public VentilationRelayController.OperatingStates DesiredState { get; set; }

			public TimeSpan TriggerTime { get; set; }

			public DayOfWeek Day { get; set; }
		}

		public class Schedule
		{
			public string Name { get; set; }

			public Dictionary<DayOfWeek, List<ScheduleState>> SchedulePerDay { get; set; }

			public XmlSchedule StoredSchedule { get; set; }

			public Schedule()
			{
				SchedulePerDay = new Dictionary<DayOfWeek, List<ScheduleState>>();

				foreach (DayOfWeek day in Enum.GetValues(typeof(DayOfWeek)))
				{
					SchedulePerDay[day] = new List<ScheduleState>();
				}
			}

			public void FixOrder()
			{
				foreach (var kvp in SchedulePerDay)
				{
					kvp.Value.Sort((left, right) =>
					{
						if (left.TriggerTime == right.TriggerTime)
							return 0;

						return left.TriggerTime > right.TriggerTime ? 1 : -1;
					});
				}
			}
		}

		public async Task HandleCommand(String command, String argument)
		{
			switch (command)
			{
				case "HighBoost":
					int timeSeconds = Convert.ToInt32(argument);
					_boostedEnd = DateTime.Now.AddSeconds(timeSeconds);
					await _ventilationController.SetTemporaryState(VentilationRelayController.OperatingStates.High, _boostedEnd);
					break;
				case "MedBoost":
					timeSeconds = Convert.ToInt32(argument);
					_boostedEnd = DateTime.Now.AddSeconds(timeSeconds);
					await _ventilationController.SetTemporaryState(VentilationRelayController.OperatingStates.Med, _boostedEnd);
					break;
				case "GetState":
					await GlobalController.SendNextScheduleState(_nextState);
					break;
				case "GetActiveSchedule":
					await SendActiveSchedule();
					break;
				case "SetSchedule":
					SetActiveSchedule(argument);
					ConfigController.SaveSchedules(AvailableSchedules.Select(s => s.Value.StoredSchedule), ActiveSchedule.Name);
					await SendActiveSchedule();
					break;
				case "GetSchedules":
					await SendAvailableSchedules();
					break;
			}

			await _ventilationController.HandleCommand(command, argument);
		}

		private async Task SendAvailableSchedules()
		{
			await GlobalController.SendAvailableSchedules(GetActiveAndAvailableSchedules());
		}

		private async Task SendActiveSchedule()
		{
			await GlobalController.SendActiveSchedule(ActiveSchedule?.Name ?? "<null>");
		}

		private IEnumerable<string> GetActiveAndAvailableSchedules()
		{
			yield return ActiveSchedule.Name;

			if (AvailableSchedules != null && AvailableSchedules.Any())
			{
				foreach (var schedule in AvailableSchedules)
				{
					yield return schedule.Key;
				}
			}
			else
			{
				yield return ActiveSchedule.Name;
			}
		}
	}
}
