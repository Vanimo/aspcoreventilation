﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AspCoreVentilation.Controllers
{
	public interface IController
    {
        Task HandleCommand(String command, String argument);

        void LoadConfig(string config);
    }
}
