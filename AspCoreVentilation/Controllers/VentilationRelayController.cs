﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Device.Gpio;
using Microsoft.AspNetCore.SignalR;

namespace AspCoreVentilation.Controllers
{
	public class VentilationRelayController : IController
	{
		private readonly Int32 _relayOne;
		private readonly Int32 _relayTwo;
		private readonly Int32 _relayThree;

		private readonly GpioController _controller;

		public OperatingStates CurrentState { get; private set; }

		private OperatingStates _scheduledState = OperatingStates.None;
		private OperatingStates _manualState = OperatingStates.None;
		public DateTime ExpectedEndOfTemporaryState { get; private set; }

		public VentilationRelayController(Int32 relayOne, Int32 relayTwo, Int32 relayThree)
		{
			_relayOne = relayOne;
			_relayTwo = relayTwo;
			_relayThree = relayThree;

			CurrentState = OperatingStates.None;
			ExpectedEndOfTemporaryState = DateTime.MinValue;
			
			_controller = new GpioController(PinNumberingScheme.Board);
			_controller.OpenPin(_relayOne, PinMode.Output);
			_controller.OpenPin(_relayTwo, PinMode.Output);
			_controller.OpenPin(_relayThree, PinMode.Output);

			SetState(OperatingStates.Off);
		}

		public VentilationRelayController()
		{
			CurrentState = OperatingStates.None;
			ExpectedEndOfTemporaryState = DateTime.MinValue;

			SetState(OperatingStates.Off);
		}

		public async Task SetScheduledState(OperatingStates state)
		{
			_scheduledState = state;

			if (ExpectedEndOfTemporaryState < DateTime.Now)
			{
				ExpectedEndOfTemporaryState = DateTime.MinValue;
				_manualState = OperatingStates.None;

				await SetState(state);
			}
		}

		public async Task SetTemporaryState(OperatingStates state, DateTime expectedEnd)
		{
			_manualState = state;
			ExpectedEndOfTemporaryState = expectedEnd;

			if (_manualState != OperatingStates.None)
			{
				await SetState(_manualState);
			}
			else
			{
				ExpectedEndOfTemporaryState = DateTime.MinValue;

				// force a recalculation of the scheduled state
				await SetScheduledState(_scheduledState);
			}
		}

		private async Task SetState(OperatingStates state)
		{
			// Note, high is off
			PinValue rOneValue = PinValue.High;
			PinValue rTwoValue = PinValue.High;
			PinValue rThreeValue = PinValue.High;

			switch (state)
			{
				case OperatingStates.Low:
					rOneValue = PinValue.Low;
					break;
				case OperatingStates.Med:
					rOneValue = PinValue.Low;
					rTwoValue = PinValue.Low;
					break;
				case OperatingStates.High:
					rOneValue = PinValue.Low;
					rTwoValue = PinValue.Low;
					rThreeValue = PinValue.Low;
					break;
				case OperatingStates.Off:
				case OperatingStates.None:
					break;
			}

			if (_controller != null)
			{
				_controller.Write(_relayOne, rOneValue);
				_controller.Write(_relayTwo, rTwoValue);
				_controller.Write(_relayThree, rThreeValue);
			}

			CurrentState = state;
			await GlobalController.SendControllerUpdate(this);
		}

		public Boolean IsInManualState => _manualState != OperatingStates.None;

		public Boolean IsInTemporaryState => ExpectedEndOfTemporaryState != DateTime.MinValue;

		public enum OperatingStates
		{
			None = -1,
			Off = 0,
			Low = 1,
			Med = 2,
			High = 3,
		}

		public async Task HandleCommand(String command, String argument)
		{
			switch (command)
			{
				case "GetState":
					await GlobalController.SendControllerUpdate(this);
					break;
				case "Manual":
					var target = (OperatingStates) Convert.ToInt32(argument);
					await SetTemporaryState(target, DateTime.MaxValue);
					break;
				case "Temporary":
					target = (OperatingStates)Convert.ToInt32(argument);
					await SetTemporaryState(target, DateTime.MinValue);
					break;
				case "Automatic":
					// clear the temporary state to go back to automatic
					await SetTemporaryState(OperatingStates.None, DateTime.MinValue);
					break;
			}
		}

        public void LoadConfig(string config)
        {
        }
    }
}
