﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;
using AspCoreVentilation.Controllers.Models;

namespace AspCoreVentilation.Controllers
{
    public class ConfigController
    {
        private const string ConfigFileName = "VentilationSettings.xml";

        public static void SaveDefaultSettings()
        {
	        var settings = GetDefaultSettings();
            SaveSettings(settings);
        }

        public static Dictionary<string, VentilationSchedulingController.Schedule> GetSavedSchedules(string config, out string lastActive)
        {
            lastActive = String.Empty;
            var result = new Dictionary<string, VentilationSchedulingController.Schedule>();
            
            var serializer = new XmlSerializer(typeof(XmlSettings));
            XmlSettings settings = ParseSettings(config);

            if (settings?.Schedules == null)
            {
	            settings = GetDefaultSettings();
                SaveDefaultSettings();
            }

            lastActive = settings.LastActiveSchedule;

            foreach (var xmlSchedule in settings.Schedules)
            {
                if (xmlSchedule?.Triggers == null)
                    continue;

                var schedule = new VentilationSchedulingController.Schedule {Name = xmlSchedule.Name, StoredSchedule = xmlSchedule};

                foreach (var xmlTrigger in xmlSchedule.Triggers)
                {
                    var state = (VentilationRelayController.OperatingStates) xmlTrigger.Level;

                    foreach (var xmlDay in xmlTrigger.DaysOfWeek)
                    {
                        var dow = ParseDayOfWeek(xmlDay);

                        schedule.SchedulePerDay[dow].Add(new VentilationSchedulingController.ScheduleState()
                        {
                            Day = dow,
                            DesiredState = state,
                            TriggerTime = xmlTrigger.TriggerTime
                        });
                    }
                }

                schedule.FixOrder();
                result.Add(xmlSchedule.Name, schedule);
            }

            return result;
        }

        public static XmlSettings GetDefaultSettings()
        {
            return new XmlSettings
            {
                Schedules = new XmlSchedule[]
                {
                    new XmlSchedule()
                    {
                        Name = "Winter",
                        Triggers = new XmlScheduleTrigger[]
                        {
                            new XmlScheduleTrigger()
                            {
                                DaysOfWeek = new[] {0, 1, 2, 3, 4, 5, 6, 7},
                                Level = 1,
                                TriggerTime = new TimeSpan(10, 0, 0)
                            },
                            new XmlScheduleTrigger()
                            {
                                DaysOfWeek = new[] {0, 1, 2, 3, 4, 5, 6, 7},
                                Level = 0,
                                TriggerTime = new TimeSpan(15, 0, 0)
                            }
                        }
                    },
                    new XmlSchedule()
                    {
                        Name = "Zomer",
                        Triggers = new XmlScheduleTrigger[]
                        {
                            new XmlScheduleTrigger()
                            {
                                DaysOfWeek = new[] {0, 1, 2, 3, 4, 5, 6, 7},
                                Level = 1,
                                TriggerTime = new TimeSpan(10, 0, 0)
                            },
                            new XmlScheduleTrigger()
                            {
                                DaysOfWeek = new[] {0, 1, 2, 3, 4, 5, 6, 7},
                                Level = 0,
                                TriggerTime = new TimeSpan(20, 0, 0)
                            }
                        }
                    },
                    new XmlSchedule()
                    {
                        Name = "Afkoelen",
                        Triggers = new XmlScheduleTrigger[]
                        {
                            new XmlScheduleTrigger()
                            {
                                DaysOfWeek = new[] {0, 1, 2, 3, 4, 5, 6, 7},
                                Level = 0,
                                TriggerTime = new TimeSpan(8, 0, 0)
                            },
                            new XmlScheduleTrigger()
                            {
                                DaysOfWeek = new[] {0, 1, 2, 3, 4, 5, 6, 7},
                                Level = 2,
                                TriggerTime = new TimeSpan(23, 0, 0)
                            }
                        }
                    },
                    new XmlSchedule()
                    {
                        Name = "Uit",
                        Triggers = new XmlScheduleTrigger[]
                        {
                            new XmlScheduleTrigger()
                            {
                                DaysOfWeek = new[] {0, 1, 2, 3, 4, 5, 6, 7},
                                Level = 0,
                                TriggerTime = new TimeSpan(0, 0, 0)
                            }
                        }
                    }
                }
            };
        }

        private static XmlSettings ParseSettings(string xmlText)
        {
	        var serializer = new XmlSerializer(typeof(XmlSettings));
	        XmlSettings settings = null;

	        try
	        {
		        using var reader = new StringReader(xmlText);
		        settings = serializer.Deserialize(reader) as XmlSettings;
	        }
	        catch
	        {
	        }

	        return settings;
        }

        public static string GetConfigFileContent()
        {
            try
            {
                return File.ReadAllText(ConfigFileName, Encoding.UTF8);
            }
            catch
            {
                return string.Empty;
            }
        }

        public static void SaveSchedules(IEnumerable<XmlSchedule> schedules, string lastActiveSchedule)
        {
	        var settings = new XmlSettings {LastActiveSchedule = lastActiveSchedule, Schedules = schedules.ToArray()};

	        SaveSettings(settings);
        }

        private static void SaveSettings(XmlSettings settings)
        {
	        var serializer = new XmlSerializer(typeof(XmlSettings));

	        using var writer = XmlWriter.Create(ConfigFileName,
		        new XmlWriterSettings()
		        {
			        Encoding = Encoding.UTF8,
			        Indent = true,
			        CloseOutput = true,
			        NewLineChars = "\n"
		        });

	        serializer.Serialize(writer, settings);
        }

        public static void OverwriteConfig(string newConfig)
        {
            try
            {
	            var config = ParseSettings(newConfig);
	            if (!(config?.Schedules?.Length > 0))
	            {
                    // invalid config
                    return;
	            }

                File.WriteAllText(ConfigFileName, newConfig, Encoding.UTF8);
            }
            catch
            {
            }
        }

        private static DayOfWeek ParseDayOfWeek(int value)
        {
            switch (value)
            {
                case 1: return DayOfWeek.Monday;
                case 2: return DayOfWeek.Tuesday;
                case 3: return DayOfWeek.Wednesday;
                case 4: return DayOfWeek.Thursday;
                case 5: return DayOfWeek.Friday;
                case 6: return DayOfWeek.Saturday;
                case 7:
                default:
                    return DayOfWeek.Sunday;
            }
        }
    }
}
