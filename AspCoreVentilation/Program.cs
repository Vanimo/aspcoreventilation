using System;
using AspCoreVentilation.Controllers;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using AspCoreVentilation.Hub;
using Microsoft.AspNetCore.SignalR;

namespace AspCoreVentilation
{
	public class Program
	{
		public static void Main(String[] args)
		{
			var host = CreateHostBuilder(args).Build();
			var hubContext = host.Services.GetService(typeof(IHubContext<CommandHub>)) as IHubContext<CommandHub>;

			GlobalController.SetHubContext(hubContext);

#if DEBUG
			GlobalController.RegisterController(0, new VentilationSchedulingController(new VentilationRelayController()));
#else
			GlobalController.RegisterController(0, new VentilationSchedulingController(new VentilationRelayController(16, 18, 22)));
			GlobalController.RegisterController(1, new RelayController(12));
#endif
			host.Run();
		}

		public static IHostBuilder CreateHostBuilder(String[] args) =>
			Host.CreateDefaultBuilder(args)
				.ConfigureWebHostDefaults(webBuilder =>
				{
					webBuilder.UseStartup<Startup>();
				});
	}
}
